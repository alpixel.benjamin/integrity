'use strict';


const {ipcRenderer, clipboard} = require('electron');
const os = require('os');
const {SHA256, SHA512} = require("sha2");
const fs = require('fs');
const $ = require('jquery')
const fileManagerBtn = document.getElementById("btn");
var resultArea = document.getElementById("result");
var resultWrapArea = document.getElementById("resultWrap");
var spinnerArea = document.getElementById("spinner");
var spinnerWait = document.getElementById("spinnerWait");
var currentHash = "";

resultWrapArea.style.display = "none";
// spinnerArea.style.display = "none";

fileManagerBtn.addEventListener('click', (event) => {
    ipcRenderer.send('open-file-dialog');
});

ipcRenderer.on('selected-directory', (event, files) => {

    resultArea.innerText = "";
    resultWrapArea.style.display = "none";
    spinnerArea.style.display = "block";
    spinnerWait.style.display = "block";
    spinnerWait.innerText = "Analyse en cours ...";

    files.forEach((path) => {
        fs.readFile(path, function (err, buf) {
            var hashedFile = SHA512(buf).toString("hex");
            resultArea.innerText = hashedFile + "\n";
            new window.Notification('Integrity', {title: 'Integrity', body: 'Analyse du fichier terminée'});
            currentHash = hashedFile;
            resultWrapArea.style.display = "block";
            spinnerArea.style.display = "none";
            spinnerWait.style.display = "none";
        });
    })
});

document.getElementById("copyCode").addEventListener('click', (event) => {
    clipboard.writeText(currentHash);
});